using System;
using DotnetNuget.SDK;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;

namespace DotnetNuget.Test
{
    [TestFixture]
    public class DotnetNugetServiceTest
    {
        private IDotnetNugetService _service;
        private ILogger<DotnetNugetService> _logger;

        [SetUp]
        public void Setup()
        {
            _logger = Substitute.For<ILogger<DotnetNugetService>>();
            _service = new DotnetNugetService(_logger);
        }

        [Test]
        public void GetWithValidInputReturnsValue()
        {
            //arrange
            var random = new Random();
            var id = random.Next(0, 1000);
            var expected = new DotnetNugetResponse { Item1 = id.ToString() };

            //act
            var result = _service.Get(id.ToString());

            //assert
            Assert.That(result.Item1, Is.EqualTo(expected.Item1));
        }
    }
}