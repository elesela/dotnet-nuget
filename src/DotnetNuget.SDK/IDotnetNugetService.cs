namespace DotnetNuget.SDK
{
    public interface IDotnetNugetService
    {
        DotnetNugetResponse Get(string id);
    }
}