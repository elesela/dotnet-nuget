using System;
using Microsoft.Extensions.Logging;

namespace DotnetNuget.SDK
{
    public class DotnetNugetService : IDotnetNugetService
    {
        private readonly ILogger<DotnetNugetService> _logger;

        public DotnetNugetService(ILogger<DotnetNugetService> logger)
        {
            _logger = logger;
        }

        public DotnetNugetResponse Get(string id)
        {
            _logger.LogDebug("Get Service Hit");

            Validate(id);

            return new DotnetNugetResponse
            {
                Item1 = id
            };
        }

        private void Validate(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                return;
            }

            if (!int.TryParse(id, out _))
            {
                throw new Exception("Invalid id");
            }

            throw new Exception("Missing id");
        }
    }
}