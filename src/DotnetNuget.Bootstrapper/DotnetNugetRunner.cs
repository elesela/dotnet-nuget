using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetNuget.Bootstrapper.Library.Bootstrapper;
using DotnetNuget.Common.Library.Common.ParserControl;
using DotnetNuget.SDK;

namespace DotnetNuget.Bootstrapper
{
    public class DotnetNugetRunner : IRunner
    {
        private readonly IDotnetNugetService _service;
        private readonly IArgumentParser _argumentParser;

        public DotnetNugetRunner(IDotnetNugetService service, IArgumentParser argumentParser)
        {
            _service = service;
            _argumentParser = argumentParser;
        }

        public Task Run(IList<string> args)
        {
            if (!_argumentParser.CanParseBody(args, out var parsed))
            {
                throw new Exception("Invalid parameters");
            }

            var item = parsed.FirstOrDefault();
            return Task.FromResult(_service.Get(item));
        }

    }
}