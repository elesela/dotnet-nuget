using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotnetNuget.Bootstrapper.Library.Bootstrapper
{
    public interface IRunner
    {
        Task Run(IList<string> args);
    }
}