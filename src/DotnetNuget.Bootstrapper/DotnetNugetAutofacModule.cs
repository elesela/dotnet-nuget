using Autofac;
using Autofac.Extensions.DependencyInjection;

namespace DotnetNuget.Bootstrapper
{
    public class DotnetNugetAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder) => builder.Populate(Registrations.Load());
    }
}