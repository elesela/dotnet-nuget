using System.Text;

namespace DotnetNuget.Common.Library.Common.ExceptionControl
{
    public class ValidationError
    {
        public string FieldName { get; set; }
        public string Description { get; set; }
        public string SubCode { get; set; }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("FieldName: ");
            stringBuilder.Append(FieldName);
            stringBuilder.Append(" Description: ");
            stringBuilder.Append(Description);
            stringBuilder.Append(" SubCode: ");
            stringBuilder.Append(SubCode);
            return stringBuilder.ToString();
        }
    }
}