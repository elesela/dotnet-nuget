using System;

namespace DotnetNuget.Common.Library.Common.SecurityControl
{
    public class ClientResponseItem
    {
        public string ClientName { get; set; }
        public string ClientId { get; set; }
        public string Domain { get; set; }
        public DateTime Created { get; set; }
        public bool? Deleted { get; set; }
    }

}