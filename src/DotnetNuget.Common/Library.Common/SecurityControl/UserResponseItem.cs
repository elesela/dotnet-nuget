using System;

namespace DotnetNuget.Common.Library.Common.SecurityControl
{
    public class UserResponseItem
    {
        public string Username { get; set; }
        public string UserId { get; set; }
        public string EmailAddress { get; set; }
        public bool? EmailVerified { get; set; }
        public bool? PasswordRequiresReset { get; set; }
        public bool? Disabled { get; set; }
        public bool? Deleted { get; set; }
        public bool? Locked { get; set; }
        public int FailedAttempts { get; set; }
        public DateTime? LastLoginAttempt { get; set; }
        public DateTime PasswordSetDate { get; set; }
    }

}