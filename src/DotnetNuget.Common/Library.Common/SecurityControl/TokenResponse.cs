using System.Collections.Generic;

namespace DotnetNuget.Common.Library.Common.SecurityControl
{
    public class TokenResponse
    {
        public IList<TokenResponseItem> Responses { get; set; }
    }
}