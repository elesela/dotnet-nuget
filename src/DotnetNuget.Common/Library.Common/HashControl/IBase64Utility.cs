namespace DotnetNuget.Common.Library.Common.HashControl
{
    public interface IBase64Utility
    {
        bool IsBase64Encoded(string val);
        string Base64Encode(string val);
        string Base64Decode(string val);
    }
}