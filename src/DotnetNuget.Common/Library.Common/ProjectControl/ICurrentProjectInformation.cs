namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public interface ICurrentProjectInformation : IProjectInformation
    {
        ICurrentEnvironmentInformation CurrentEnvironment { get; set; }
    }
}