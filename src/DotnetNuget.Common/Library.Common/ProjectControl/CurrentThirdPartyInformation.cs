namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public class CurrentThirdPartyInformation : ICurrentThirdPartyInformation
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public string Key { get; set; }
        public string Domain { get; set; }
        public string TenantId { get; set; }
        public string ClientId { get; set; }
    }
}