namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public interface IEnvironmentInformation
    {
        ICurrentEnvironmentInformation Production { get; set; }
        ICurrentEnvironmentInformation Staging { get; set; }
        ICurrentEnvironmentInformation Uat { get; set; }
        ICurrentEnvironmentInformation Development { get; set; }
        ICurrentEnvironmentInformation Local { get; set; }
    }
}