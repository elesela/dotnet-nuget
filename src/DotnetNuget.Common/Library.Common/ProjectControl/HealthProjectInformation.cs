namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public class HealthProjectInformation : IHealthProjectInformation
    {
        public string ApplicationName { get; set; }
        public ICurrentVersionInformation VersionInformation { get; set; }
        public IHealthEnvironmentInformation CurrentEnvironment { get; set; }
    }
}