using DotnetNuget.Common.Library.Common.EnvironmentControl;

namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public class AppSettings
    {
        public const string AppSettingsSectionName = "CurrentProjectInformation";
        public string ApplicationName { get; set; }
        public AppSettingsVersion VersionInformation { get; set; }
        public AppSettingsEnvironment EnvironmentInformation { get; set; }
    }

    public class AppSettingsVersion
    {
        public string ApiVersion { get; set; }
        public string ProjectVersion { get; set; }
        public string GitCommitHash { get; set; }
    }

    public class AppSettingsEnvironment
    {
        public AppSettingsCurrentEnvironment Production { get; set; }
        public AppSettingsCurrentEnvironment Staging { get; set; }
        public AppSettingsCurrentEnvironment Uat { get; set; }
        public AppSettingsCurrentEnvironment Development { get; set; }
        public AppSettingsCurrentEnvironment Local { get; set; }
    }

    public class AppSettingsCurrentEnvironment
    {
        public string Url { get; set; }
        public EnvironmentEnum Environment { get; set; }
        public string Name { get; set; }
        public AppSettingsThirdParty Authentication { get; set; }
    }

    public class AppSettingsThirdParty
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public string Key { get; set; }
        public string Domain { get; set; }
        public string TenantId { get; set; }
        public string ClientId { get; set; }
    }
}