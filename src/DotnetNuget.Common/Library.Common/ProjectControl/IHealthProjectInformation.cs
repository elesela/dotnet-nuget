namespace DotnetNuget.Common.Library.Common.ProjectControl
{
    public interface IHealthProjectInformation : IProjectInformation
    {
        IHealthEnvironmentInformation CurrentEnvironment { get; set; }
    }
}