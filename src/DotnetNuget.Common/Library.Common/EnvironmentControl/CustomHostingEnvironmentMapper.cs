using System;

namespace DotnetNuget.Common.Library.Common.EnvironmentControl
{
    public class CustomHostingEnvironmentMapper : ICustomHostingEnvironmentMapper
    {
        private readonly IEnvironmentVariableUtility _environmentVariableUtility;
        private readonly string _applicationName;

        public CustomHostingEnvironmentMapper(string applicationName, IEnvironmentVariableUtility environmentVariableUtility)
        {
            _environmentVariableUtility = environmentVariableUtility;
            _applicationName = applicationName;
        }

        public ICustomHostingEnvironment Get()
        {
            var environmentName = _environmentVariableUtility.Get("ASPNETCORE_ENVIRONMENT");

            if (string.IsNullOrWhiteSpace(environmentName))
            {
                return new CustomHostingEnvironment(_applicationName, EnvironmentEnum.Unknown);
            }

            environmentName = environmentName.ToLower().Trim();

            if ("local".Equals(environmentName, StringComparison.OrdinalIgnoreCase))
            {
                return new CustomHostingEnvironment(_applicationName, EnvironmentEnum.Local);
            }

            if ("development".Equals(environmentName, StringComparison.OrdinalIgnoreCase))
            {
                return new CustomHostingEnvironment(_applicationName, EnvironmentEnum.Development);
            }

            if ("uat".Equals(environmentName, StringComparison.OrdinalIgnoreCase))
            {
                return new CustomHostingEnvironment(_applicationName, EnvironmentEnum.Uat);
            }

            if ("staging".Equals(environmentName, StringComparison.OrdinalIgnoreCase))
            {
                return new CustomHostingEnvironment(_applicationName, EnvironmentEnum.Staging);
            }

            if ("production".Equals(environmentName, StringComparison.OrdinalIgnoreCase))
            {
                return new CustomHostingEnvironment(_applicationName, EnvironmentEnum.Production);
            }

            return new CustomHostingEnvironment(_applicationName, EnvironmentEnum.Unknown);
        }
    }
}