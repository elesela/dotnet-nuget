namespace DotnetNuget.Common.Library.Common.MessageControl
{
    public interface IPaginationResponse
    {
        int PageIndex { get; }
        int PageSize { get; }
        int TotalCount { get; }
        int TotalPages { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
    }
}