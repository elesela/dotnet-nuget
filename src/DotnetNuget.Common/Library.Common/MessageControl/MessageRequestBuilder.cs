using System;

namespace DotnetNuget.Common.Library.Common.MessageControl
{
    public class MessageRequestBuilder
    {
        private readonly string _applicationId;
        private readonly Guid _messageId;
        private readonly DateTimeOffset _startTime;
        private Guid? _callerId;
        private string _clientMessageId;
        private string _callerAppId;
        private string _ipAddress;
        private bool _cacheControl;
        private bool _isCached;
        private string _groupId;
        private ServiceTypeEnum _serviceType;
        private string _searchKey;
        private bool _excludeFromBilling;
        private long? _batchId;
        private Guid? _transactionId;

        public MessageRequestBuilder(Guid messageId, string applicationId, DateTimeOffset startTime)
        {
            _messageId = messageId;
            _applicationId = applicationId;
            _startTime = startTime;
        }

        public MessageRequestBuilder CallerId(Guid? callerId)
        {
            _callerId = callerId;
            return this;
        }

        public MessageRequestBuilder ClientMessageId(string clientMessageId)
        {
            _clientMessageId = clientMessageId;
            return this;
        }

        public MessageRequestBuilder CallerAppId(string callerAppId)
        {
            _callerAppId = callerAppId;
            return this;
        }

        public MessageRequestBuilder IpAddress(string ipAddress)
        {
            _ipAddress = ipAddress;
            return this;
        }

        public MessageRequestBuilder CacheControl(bool cacheControl)
        {
            _cacheControl = cacheControl;
            return this;
        }

        public MessageRequestBuilder Cached(bool isCached)
        {
            _isCached = isCached;
            return this;
        }

        public MessageRequestBuilder GroupId(string groupId)
        {
            _groupId = groupId;
            return this;
        }

        public MessageRequestBuilder ServiceType(string serviceType)
        {
            var isServiceType = Enum.TryParse(serviceType, true, out ServiceTypeEnum serviceTypeEnum);

            _serviceType = isServiceType ? serviceTypeEnum : ServiceTypeEnum.Api;
            return this;
        }

        public MessageRequestBuilder SearchKey(string searchKey)
        {
            _searchKey = searchKey;
            return this;
        }

        public MessageRequestBuilder ExcludeFromBilling(bool excludeFromBilling)
        {
            _excludeFromBilling = excludeFromBilling;
            return this;
        }

        public MessageRequestBuilder BatchId(long? batchId)
        {
            _batchId = batchId;
            return this;
        }

        public MessageRequestBuilder TransactionId(Guid? transactionId)
        {
            _transactionId = transactionId;
            return this;
        }

        public IMessageRequest Build() =>
            new MessageRequest(_messageId, _callerId, _clientMessageId, _callerAppId, _applicationId, _ipAddress,
                _startTime, _cacheControl, _isCached, _groupId, _serviceType, _searchKey, _excludeFromBilling,
                _transactionId, _batchId);
    }
}