using System;
using System.IO;
using DotnetNuget.Bootstrapper.Library.Bootstrapper;
using DotnetNuget.Common;
using DotnetNuget.Common.Library.Common.EnvironmentControl;
using DotnetNuget.Common.Library.Common.LogControl;
using DotnetNuget.Common.Library.Common.ProjectControl;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace DotnetNuget.ConsoleApplication
{
    internal class Program
    {
        private static readonly IEnvironmentVariableUtility EnvironmentVariableUtility;
        private static readonly ILogLocationUtility LogLocationUtility;
        private static readonly string ApplicationName;
        private static readonly string CurrentApiVersion;
        private static readonly string DefaultLogLocation;

        static Program()
        {
            EnvironmentVariableUtility = new EnvironmentVariableUtility();
            LogLocationUtility = new LogLocationUtility(EnvironmentVariableUtility);
            ApplicationName = DotnetNugetConstants.ApplicationName;
            CurrentApiVersion = DotnetNugetConstants.CurrentApiVersion();
            DefaultLogLocation = DotnetNugetConstants.DefaultLogLocation;
        }

        public static void Main(string[] args)
        {
            var logger = LoggerSetup.Initialize(LogLocationUtility.GetLogLocation(DefaultLogLocation, ApplicationName));

            try
            {
                Log.Information("Started program");

                var projectInformation = GetCurrentProjectInformation(CurrentApiVersion, EnvironmentVariableUtility);

                var services = new ServiceCollection()
                    .AddSingleton(projectInformation)
                    .AddSingleton(new ConsoleHostingEnvironment(projectInformation))
                    .AddLogging(builder => builder.AddSerilog(logger));

                new StartupRunner().Run(args, services);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static ICurrentProjectInformation GetCurrentProjectInformation(string currentApiVersion, IEnvironmentVariableUtility environmentVariableUtility)
        {
            var appSettings = GetConfigurationRoot()
                .GetSection(AppSettings.AppSettingsSectionName)
                .Get<AppSettings>();

            var environment = new CustomHostingEnvironmentMapper(appSettings.ApplicationName, environmentVariableUtility).Get();

            return new ProjectInformationMapper().MapCurrent(appSettings, currentApiVersion, environment.Environment);
        }

        private static IConfiguration GetConfigurationRoot(IConfiguration configuration = null)
        {
            if (configuration != null)
            {
                return new ConfigurationBuilder()
                    .AddConfiguration(configuration)
                    .AddJsonFile("appsettings.json", false, false)
                    .AddEnvironmentVariables().Build();
            }

            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false)
                .AddEnvironmentVariables().Build();
        }
    }
}